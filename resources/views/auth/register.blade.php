<x-guest-layout>
    <x-validation-errors class="mb-4" />

    <div class="container">
        <div class="img">
            <img src="{{ asset('assets/img/1.png') }}" alt="Image">
        </div>
        <div class="login-content">
            <form method="POST" action="{{ route('register') }}">
                @csrf
                <!-- <img src="img/avatar.svg"> -->
                <h2 class="title">register</h2>
                <div class="input-div one">
                    <div class="i"></div>
                    <div class="div">
                        <x-input placeholder="Name" id="name" class="block w-full mt-1" type="text"
                            name="name" :value="old('name')" required autofocus autocomplete="name" />
                    </div>
                </div>
                <div class="input-div one">
                    <div class="i"></div>
                    <div class="div">
                        <x-input placeholder="Email" id="email" class="block w-full mt-1" type="email"
                            name="email" :value="old('email')" required autocomplete="username" />
                    </div>
                </div>
                <div class="input-div one">
                    <div class="i"></div>
                    <div class="div">
                        <x-input placeholder="Password" id="password" class="block w-full mt-1" type="text"
                            name="password" required autocomplete="new-password" />
                    </div>
                </div>
                <div class="input-div pass">
                    <div class="i"></div>
                    <div class="div">
                        <x-input placeholder="Confirm Password" id="password_confirmation" class="block w-full mt-1"
                            type="text" name="password_confirmation" required autocomplete="new-password" />
                    </div>
                </div>
                <!-- <a href="#">Forgot Password?</a> -->
                <x-button class="btn">
                    {{ __('Register') }}
                </x-button>
            </form>
        </div>
    </div>
</x-guest-layout>
