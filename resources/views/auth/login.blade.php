<x-guest-layout>

    <body>
        <x-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 text-sm font-medium text-green-600">
                {{ session('status') }}
            </div>
        @endif
        <!-- <img class="wave" src="img/wave.png"> -->
        <div class="container">
            <div class="img">
                <img src="{{ asset('assets/img/1.png') }}" alt="Image">
            </div>
            <div class="login-content">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <!-- <img src="img/avatar.svg"> -->
                    <h2 class="title">Log In</h2>
                    <div class="input-div one">
                        <div class="i">
                            <i class="fas fa-user"></i>
                        </div>
                        <div class="div">
                            <x-input placeholder="Email" id="email" class="block w-full mt-1" type="email"
                                name="email" :value="old('email')" required autofocus autocomplete="username" />
                        </div>
                    </div>
                    <div class="input-div pass">
                        <div class="i">
                            <i class="fas fa-lock"></i>
                        </div>
                        <div class="div">
                            <x-input placeholder="Password" id="password" class="block w-full mt-1" type="password"
                                name="password" required autocomplete="current-password" />
                        </div>
                    </div>
                    <!-- <a href="#">Forgot Password?</a> -->
                    <button class="btn">
                        {{ __('Log in') }}
                    </button>
                </form>
            </div>
        </div>
</x-guest-layout>
